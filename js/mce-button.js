(function () {
  tinymce.PluginManager.add('cl_shortcode', function (editor, url) {
    var sh_tag = 'listings';
    //helper functions 
    function getAttr(s, n) {
      n = new RegExp(n + '=\"([^\"]+)\"', 'g').exec(s);
      return n ? window.decodeURIComponent(n[1]) : '';
    }
    ;
    function html(cls, data, con) {
      var placeholder = url + '/img/' + getAttr(data, 'type') + '.jpg';
      data = window.encodeURIComponent(data);
      content = window.encodeURIComponent(con);
      return '<img src="' + placeholder + '" class="mceItem ' + cls + '" ' + 'data-sh-attr="' + data + '" data-sh-content="' + con + '" data-mce-resize="false" data-mce-placeholder="1" />';
    }

    function replaceShortcodes(content) {
      return content.replace(/\[bs3_panel([^\]]*)\]([^\]]*)\[\/bs3_panel\]/g, function (all, attr, con) {
        return html('wp-bs3_panel', attr, con);
      });
    }

    function restoreShortcodes(content) {
      return content.replace(/(?:<p(?: [^>]+)?>)*(<img [^>]+>)(?:<\/p>)*/g, function (match, image) {
        var data = getAttr(image, 'data-sh-attr');
        var con = getAttr(image, 'data-sh-content');
        if (data) {
          return '<p>[' + sh_tag + data + ']' + con + '[/' + sh_tag + ']</p>';
        }
        return match;
      });
    }

    //add popup
    editor.addCommand('cl_shortcode_popup', function (ui, v) {
      //setup defaults
      var ajax;
      ajax = jQuery.ajax({
        url: 'admin-ajax.php',
        data: {
          'action': 'do_ajax'
        },
        dataType: 'json'
//        success: function (data) {
//          if (data) {
//            console.log(data);
//            return data;
//          }
//          else {
//            console.log('error ajax');
//          }
//        },
//        error: function(a,b,c){
//          console.log(a);
//          console.log(b);
//          console.log(c);
//        }
      });
      ajax.success(function (data) {
        options = [];
        jQuery.each(data, function (index, val) {
          options.push({
            selected: false,
            text: index,
            value: val
          });
        });
        
        var win = editor.windowManager.open({
          title: 'Creative Listing Shortcode',
          body: [
            {
              type: 'listbox',
              name: 'list_from',
              label: 'List From',
              onselect: function () {
                if (this.value() === 'coordinates') {
                  jQuery('#id_latitude').prop('disabled', false);
                  jQuery('#id_latitude-l').css('color', 'inherit');
                  jQuery('#id_latitude').focus();
                  jQuery('#id_longitude').prop('disabled', false);
                  jQuery('#id_longitude-l').css('color', 'inherit');
                  jQuery('#id_zip_code').prop('disabled', true);
                  jQuery('#id_base_address').prop('disabled', true);
                }
                if (this.value() === 'zipcode') {
                  jQuery('#id_zip_code').prop('disabled', false);
                  jQuery('#id_zip_code-l').css('color', 'inherit');
                  jQuery('#id_zip_code').focus();
                  jQuery('#id_latitude').prop('disabled', true);
                  jQuery('#id_longitude').prop('disabled', true);
                  jQuery('#id_base_address').prop('disabled', true);
                }
                if (this.value() === 'baseaddress') {
                  jQuery('#id_base_address').prop('disabled', false);
                  jQuery('#id_base_address-l').css('color', 'inherit');
                  jQuery('#id_base_address').focus();
                  jQuery('#id_latitude').prop('disabled', true);
                  jQuery('#id_longitude').prop('disabled', true);
                  jQuery('#id_zip_code').prop('disabled', true);
                }
              },
              value: '',
              'values': [
                {text: 'Select', value: ''},
                {text: 'Coordinates', value: 'coordinates'},
                {text: 'Zip Code', value: 'zipcode'},
                {text: 'Base Address', value: 'baseaddress'}
              ],
              tooltip: 'Select one of the list'
            },
            {
              type: 'textbox',
              name: 'latitude',
              label: 'Latitude',
              value: '',
              disabled: true,
              id: 'id_latitude',
              classes: 'latitude',
              tooltip: 'Latitude'
            },
            {
              type: 'textbox',
              name: 'longitude',
              label: 'Longitude',
              value: '',
              disabled: true,
              id: 'id_longitude',
              class: 'longitude',
              tooltip: 'Longitude'
            },
            {
              type: 'textbox',
              name: 'zip_code',
              label: 'Zip Code',
              value: '',
              disabled: true,
              id: 'id_zip_code',
              class: 'zip_code',
              tooltip: 'Zip Code'
            },
            {
              type: 'textbox',
              name: 'base_address',
              label: 'Base Address',
              value: '',
              id: 'id_base_address',
              class: 'base_address',
              tooltip: 'Base Address'
            },
            {
              type: 'listbox',
              name: 'taxonomy',
              label: 'Filter by Taxonomy',
              id: 'id_taxonomy',
              //value: '',
              'values': options,
              tooltip: 'Select the taxonomy'
            },
            {
              type: 'textbox',
              name: 'term',
              label: 'Filter by Term',
              value: '',
              id: 'id_term',
              class: 'term',
              tooltip: 'Term'
            },
            {
              type: 'checkbox',
              name: 'showdistance',
              id: 'id_showdistance',
              label: 'Show Distane',
              value: '1',
            },
            {
              type: 'checkbox',
              name: 'ignore_weight',
              id: 'id_ignore_weight',
              label: 'Ignore Weight',
              value: '1',
            },
            {
              type: 'listbox',
              name: 'limit_by',
              label: 'Limit By',
              value: '',
              'values': [
                {text: 'Search Radius', value: 'search_radius'},
                {text: 'Limit items by quantity', value: 'quantity'}
              ],
              tooltip: 'Select the Limit'
            },
            {
              type: 'textbox',
              name: 'limit',
              label: 'Limit',
              value: '',
              id: 'id_limit',
              class: 'limit',
              tooltip: 'Limit'
            },
            {
              type: 'textbox',
              name: 'specific_items',
              label: 'List Specific Items',
              value: '',
              class: 'specific_items',
              tooltip: 'List Specific Items'
            },
            {
              type: 'textbox',
              name: 'callback_function',
              label: 'Callback Function Name',
              value: '',
              class: 'callback_function',
              tooltip: 'Callback Function Name'
            }
          ],
          onsubmit: function (e) {
            var shortcode_str = '[' + sh_tag;
            //check for header
            if (typeof e.data.list_from !== 'undefined' && e.data.list_from !== '')
              if (e.data.list_from === 'coordinates')
                shortcode_str += ' latitude="' + e.data.latitude + '", longitude="' + e.data.longitude + '"';
            if (e.data.list_from === 'zipcode')
              shortcode_str += ' zip_code="' + e.data.zip_code + '"';
            if (e.data.list_from === 'baseaddress')
              shortcode_str += ' base_address="' + e.data.base_address + '"';
            if (typeof e.data.taxonomy !== 'undefined' && e.data.taxonomy !== '')
              shortcode_str += ' taxonomy="' + e.data.taxonomy + '"';
            if (e.data.term !== '')
              shortcode_str += ' term="' + e.data.term + '"';
            if (e.data.showdistance === true)
              shortcode_str += ' showdistance="true"';
            if (e.data.ignore_weight === true)
              shortcode_str += ' ignore_weight="true"';
            if (e.data.limit_by !== '')
              if (e.data.list_from === 'search_radius')
                shortcode_str += ' search_radius="' + e.data.limit + '"';
            if (e.data.list_from === 'quantity')
              shortcode_str += ' limit="' + e.data.limit + '"';
            if (e.data.specific_items !== '')
              shortcode_str += ' ' + e.data.specific_items;
            if (e.data.callback_function !== '')
              shortcode_str += ' callback_query="' + e.data.callback_function + '"';
            shortcode_str += ']';
            //insert shortcode to tinymce
            editor.insertContent(shortcode_str);
          }
        });
      });
    });
    //add button
    editor.addButton('cl_shortcode', {
      icon: 'bs3_panel',
      tooltip: 'Creative Listing Shortcode',
      onclick: function () {
        editor.execCommand('cl_shortcode_popup', '', {
          header: '',
          footer: '',
          type: 'default',
          content: ''
        });
      }
    });
    //replace from shortcode to an image placeholder
    editor.on('BeforeSetcontent', function (event) {
      event.content = replaceShortcodes(event.content);
    });
    //replace from image placeholder to shortcode
    editor.on('GetContent', function (event) {
      event.content = restoreShortcodes(event.content);
    });
    //open popup on placeholder double click
    editor.on('DblClick', function (e) {
      var cls = e.target.className.indexOf('wp-bs3_panel');
      if (e.target.nodeName == 'IMG' && e.target.className.indexOf('wp-bs3_panel') > -1) {
        var title = e.target.attributes['data-sh-attr'].value;
        title = window.decodeURIComponent(title);
        console.log(title);
        var content = e.target.attributes['data-sh-content'].value;
        editor.execCommand('cl_shortcode_popup', '', {
          header: getAttr(title, 'header'),
          footer: getAttr(title, 'footer'),
          type: getAttr(title, 'type'),
          content: content
        });
      }
    });
  });
})();