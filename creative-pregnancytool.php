<?php
/*
Plugin Name: Creative Listings
Depends: Pronamic Google Maps
Version: 1.0
Author: Creative Edition
Author URI: http://creative-edition.co.uk
*/


if(!class_exists('CL_Plugin'))
{
	class CL_Plugin
	{
		/**
		 * Construct the plugin object
		 */
		public function __construct()
		{
			// Initialize Settings
			define('PLUGIN_DIR_PATH', plugin_dir_path(__FILE__));
			
			require_once(sprintf("%s/settings.php", dirname(__FILE__)));
			CL_Plugin_Settings::init();
			
			require_once(sprintf("%s/classes/creative-listings-shortcodes.php", dirname(__FILE__)));
			CL_Plugin_Shortcodes::bootstrap();

			require_once(sprintf("%s/listings-modal.class.php",	dirname(__FILE__)));
			new CL_Plugin_Modal();
			
			add_action("wp_ajax_get_listings", array( $this , 'get_listings' ) );
			add_action("wp_ajax_nopriv_get_listings", array( $this , 'get_listings' ) );

		} // END public function __construct

		/**
		 * Activate the plugin
		 */
		public static function activate()
		{
			if( !is_plugin_active( 'pronamic-google-maps/pronamic-google-maps.php' ) ) {
				wp_die( 'Plugins depends on Pronamic Google Maps', 'Error' );
			}
		} // END public static function activate

		/**
		 * Deactivate the plugin
		 */
		public static function deactivate()
		{
			// Do nothing
		} // END public static function deactivate

		// Add the settings link to the plugins page
		function plugin_settings_link($links)
		{
			$settings_link = '<a href="options-general.php?page=listings_template">Settings</a>';
			array_unshift($links, $settings_link);
			return $links;
		}

		function get_listings(){
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				echo json_encode(
						array(
								'type' => 'success',
								'html' => do_shortcode( '[listings latitude="'.sanitize_text_field( $_POST['latitude'] ).'" longitude="'.sanitize_text_field( $_POST['longitude'] ).'" search_radius="999000" showdistance="true" ignore_weight="true"]' )
							)
					);
			}
			else {
				header("Location: ".$_SERVER["HTTP_REFERER"]);
			}
			die();
		}

		function cl_after_wp_tiny_mce(){
			// if ( 'edit.php' != $hook ) {
			// 	return;
			// }
			 printf( '<script type="text/javascript" src="%s"></script>',  plugins_url('/js/modal-window-init.js', __FILE__) );
			// wp_enqueue_script( 'creative-listings-admin-modal', plugins_url('/js/modal-window-init.js', __FILE__), array('jquery'), false, true);;			
		}
	} // END class CL_Plugin
} // END if(!class_exists('CL_Plugin'))

if(class_exists('CL_Plugin'))
{
	// Installation and uninstallation hooks
	register_activation_hook(__FILE__, array('CL_Plugin', 'activate'));
	register_deactivation_hook(__FILE__, array('CL_Plugin', 'deactivate'));

	// instantiate the plugin class
	$wp_plugin_template = new CL_Plugin();

}
